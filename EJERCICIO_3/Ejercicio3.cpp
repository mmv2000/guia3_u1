#include <iostream>
#include "Lista.h"

using namespace std;

void completar(Lista *lista) {
  Nodo *temp = lista->get_primero();
  Nodo *temporal;
  int nuevo;

  temporal = temp->sgt;

  while(temporal != NULL){
    if(temporal->numero != temp->numero + 1){
      nuevo = temp->numero + 1;
      lista->crear_nodo(nuevo);
    }
  temp = temporal;
  temporal = temporal->sgt;
  }
}

int main(int argc, char const *argv[]) {

    // crea la lista
    Lista *lista =  new Lista();
    int opcion;
    int numero;

    cout << "PROGRAMA PARA CREAR LISTA ENLAZADA" << endl;

    do{
        cout << "INGRESE UN NUMERO:";
        cin >> numero;

        // creada la lista, se agregan los elementos
        lista->crear_nodo(numero);

        lista->imprimir_lista();

        cout << endl;

        cout << "PARA CONTINUAR AGREGANDO NUMEROS PRESIONE 1" << endl;
        cout << "PARA SALIR PRESIONE 0" << endl;
        cin >> opcion;
    }
    while(opcion != 0);

    completar(lista);

    cout << "ASI QUEDO LA LISTA:" << endl;
    lista->imprimir_lista();

    return 0;
}
