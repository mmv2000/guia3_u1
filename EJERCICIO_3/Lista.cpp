#include <iostream>

//Definición de la clase
#include "Lista.h"

using namespace std;

Lista::Lista() {}

void Lista::crear_nodo(int dato) {
  Nodo *temp = new Nodo;

  temp->numero = dato;
  temp->sgt = NULL;

  if(this->primero == NULL) {
     this->primero = temp;
     this->ultimo = this->primero;
  }
  else{
    this->ultimo->sgt = temp;
    this->ultimo = temp;
  }

  ordenar(this->primero);
}

void Lista::ordenar(Nodo *temp){
    // auxiliar para metodo burbuja para comparar si es mayor o menor
    int aux;

    while(temp != NULL){
        Nodo *temporal = temp->sgt;
            while(temporal != NULL){
                if(temp->numero > temporal->numero){
                    aux = temporal->numero;
                    temporal->numero = temp->numero;
                    temp->numero = aux;
                }
            temporal = temporal->sgt;
        }
    temp = temp->sgt;
    }
}

// imprime la lista ordenada
void Lista::imprimir_lista(){
    Nodo *temp = this->primero;

    while (temp != NULL) {
        cout << temp->numero << "|";
        temp = temp->sgt;
    }
    cout << endl;
}

// metodo para recorrer lista desde el primer dato
Nodo* Lista::get_primero(){
    return this->primero;
}
