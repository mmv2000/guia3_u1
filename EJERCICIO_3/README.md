# Ejercicio3 - Guia 3 Listas enlazadas simples
Se solicita escribir un programa que complete una lista de numeros ingresados y que esta almacene
todos los valores que se encuentran a partir del número del primer nodo hasta el útimo del último 
nodo. Como ejemplo tengo una lista que contiene los numeros: 10 - 11 - 15 - 16 - 20, por tanto la 
lista final guardará los números: 10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18 - 19 - 20, es decir
los números restantes que se encuentran entre el 10 (primer número del primer nodo) hasta el 20
(último número del último nodo).

# Como funciona
El programa inicia con la solicitud de ingresar un número de tipo entero a la lista, una vez ingresado
el número se da la opción de seguir agregando números, o bien puede salir con las opción 0. Una vez 
ingresados los números el programa va evaluando constantemente los numeros ingresados, para de una vez
ordenarlos por orden de menor a mayor. Finalmente una vez finlaizada la acción del ususario ingresando núemros 
al presionar 0 el programa arroja la lista final ordenada de los números que se encuentran entre el menor valor
hasta el mayor, ambos ingresados por el ususario.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta de los
archivos donde se almacenaron. Prontamente se debe ejecutar el comando make en la terminal para la respectiva 
compilación de los archivos y luego ejecutar el comando ./Ejercicio3 para iniciar el programa una vez ya compilado.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
