#include <iostream>

//Definición de la clase
#include "Lista.h"

using namespace std;

Lista::Lista() {}

// crea nodo
void Lista::crear_nodo(int dato){
    Nodo *temp = new Nodo;

    // se apunta al nodo siguiente y genera espacio de memoria
    temp->numero = dato;
    temp->sgt = NULL;

    // Si es el primer nodo se deja como raiz y último a dicho nodo
    if (this->primero == NULL) {
        this->primero = temp;
        this->ultimo = this->primero;
    }

    // Si no, deja al actual nodo como primero y al nuevo como el ultimo
    else {
        this->ultimo->sgt = temp;
        this->ultimo = temp;
    }

   ordenar(this->primero);
}

// ordena de menor a mayor
void Lista::ordenar(Nodo *temp){
    // auxiliar para metodo burbuja para comparar si es mayor o menor
    int aux;
    
    while(temp != NULL){
        Nodo *temporal = temp->sgt;
            while(temporal != NULL){
                if(temp->numero > temporal->numero){
                    aux = temporal->numero;
                    temporal->numero = temp->numero;
                    temp->numero = aux;
                }
            temporal = temporal->sgt;
        }
    temp = temp->sgt;
    }

}

// imprime la lista ordenada
void Lista::imprimir_lista(){
    Nodo *temp = this->primero;

    cout << "LISTA ORDENADA:" << endl;

    while (temp != NULL) {
        cout << temp->numero << "|";
        temp = temp->sgt;
    }
}
