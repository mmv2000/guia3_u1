# Ejercicio1 - Guia 3 Listas enlazadas simples
Se solicita escribir un programa que solicite numeros de tipo entero y los vaya ingresando a una 
lista enlazada simple ordenada. Por cada ingreso debe mostrar el estado de la lista.

# Como funciona
El programa inicia con la solicitud de ingresar un número de tipo entero a la lista, una vez ingresado
el número se da la opción de seguir agregando números (lo cual sería lo adecuado para observar el resultado
del programa), o bien puede salir con las opciones 1 y 0 respectivamente. Una vez ingresados los números 
el programa va evaluando constantemente los numeros ingresados, para de una vez ordenarlos por orden de menor
a mayor, mostrando a la vez como va quedando la lista por cada ingreso como se solicita en el ejercicio.
Finalmente una vez finlaizada la interacción del ususario al presionar 0 el programa arroja la lista final ordenada.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta de los
archivos donde se almacenaron. Prontamente se debe ejecutar el comando make en la terminal para la respectiva
compilación de los archivos y luego ejecutar el comando ./Ejercicio1 para iniciar el programa una vez ya compilado.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
