#include <iostream>
#include "Ejercicio1.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

// clase
class Lista {
    private:
      Nodo *primero = 0;
      Nodo *ultimo = 0;

    public:
      // funciones
      Lista();
      void crear_nodo(int dato);
      void ordenar(Nodo *temp);
      void imprimir_lista();
};
#endif
