#include <iostream>
#include "Lista.h"

using namespace std;

int main(int argc, char const *argv[]) {

    // crea la lista
    Lista *lista =  new Lista();
    int opcion;
    int numero;

    cout << "PROGRAMA PARA CREAR LISTA ENLAZADA" << endl;

    do{
        cout << "INGRESE UN NUMERO:";
        cin >> numero;

        // creada la lista, se agregan los elementos
        lista->crear_nodo(numero);

        // imprime por cada número ingresado
        lista->imprimir_lista();

        cout << endl;

        cout << "PARA CONTINUAR AGREGANDO NUMEROS PRESIONE 1" << endl;
        cout << "PARA SALIR PRESIONE 0" << endl;
        cin >> opcion;
    }
    while(opcion != 0);

    // lista final una vez finalizado el ingreso de datos
    cout << "ASI QUEDO LA LISTA:" << endl;
    lista->imprimir_lista();

    return 0;
}
