# Ejercicio2 - Guia 3 Listas enlazadas simples
Se pide escribir un programa que genere dos listas enlazadas ordenadas (lea los datos) y formar una tercera
lista que resulte de la mezcla de los elementos de ambas listas.finalmente mostrar el contenido de las
tres listas generadas.

# Como funciona
El programa inicia con un menú de tres opciones que consisten en: 1 agregar numeros a las listas, 2 ver el 
estado de las listas uno dos y tres respectivamente y opción 3 que es para abandonar el programa. Una vez ingresada
la opción 1, en este caso se preguntará en que lista se desea agregar números, puede ser en la lista 1 o lista 2
y se procederá a ingresar numeros tantas veces quiera hasta que desee el ususario abandonar presionando la tecla 0.
Ya abandonada la opción de ingresar números (lista 1 o lista 2) el programa ofrece observar el contenido de las listas
1, 2 y 3 mediante la tecla 2. La opción 3 permite salir del programa.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir una terminal en la carpeta de los
archivos donde se almacenaron. Prontamente se debe ejecutar el comando make en la terminal para la respectiva
compilación de los archivos y luego ejecutar el comando ./Ejercicio2 para iniciar el programa una vez ya compilado.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
