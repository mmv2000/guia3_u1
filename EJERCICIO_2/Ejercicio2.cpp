#include <iostream>
#include "Lista.h"

using namespace std;

// funcion combina listas ya sea 1 o 2, agregando a la lista 3
void combinar(Lista *lista, Lista *lista_3) {
  int valor;

  // reconoce el primer elemento de la lista 1 o 2
  Nodo *temp = lista->get_primero();

  while(temp != NULL){
    // agrega datos de las listas creadas (1 y 2) a la lista 3
    valor = temp->numero;
    lista_3->crear_nodo(valor);
    temp = temp->sgt;
  }
}

// desarrollo ejercicio
void menu(Lista *lista_1, Lista *lista_2, Lista *lista_3){
  int op;
  int numero;
  int eleccion;
  int salir;

  cout << "PARA AGREGAR NUMEROS PRESIONE 1" << endl;
  cout << "PARA VER LISTAS PRESIONE 2" << endl;
  cout << "PARA SALIR PRESIONE 3" << endl;

  cout << "INGRESE SU OPCION:" << endl;
  cin >> op;

  switch (op) {
    /* primer caso se agregan elementos a la lista ya sea 1 o 2,
    dependiendo de la eleccion del usuario */
    case 1:
        cout << "PRESIONE 1 PARA AGREGAR NUMEROS A LA LISTA 1" << endl;
        cout << "PRESIONE 2 PARA AGREGAR NUMEROS A LA LISTA 2" << endl;
        cin >> eleccion;

        if(eleccion == 1){
          do{
            // elementos para lista 1
            cout << "INGRESE UN NUMERO:";
            cin >> numero;

            // creada la lista, se agregan los elementos
            lista_1->crear_nodo(numero);

            cout << "PARA CONTINUAR AGREGANDO NUMEROS PRESIONE 1" << endl;
            cout << "PARA SALIR PRESIONE 0" << endl;
            cin >> salir;
          }
          while(salir != 0);

          // agrega elementos a lista 3 ordenados
          combinar(lista_1, lista_3);
          // una vez agregados se vuelve a la instancia de eleccion
          menu(lista_1, lista_2, lista_3);
        }

        else if (eleccion == 2){
          do{
            cout << "INGRESE UN NUMERO:";
            cin >> numero;

            // creada la lista, se agregan los elementos
            lista_2->crear_nodo(numero);

            cout << "PARA CONTINUAR AGREGANDO NUMEROS PRESIONE 1" << endl;
            cout << "PARA SALIR PRESIONE 0" << endl;
            cin >> salir;
          }
          while(salir != 0);

          // agrega elementos a lista 3 ordenados
          combinar(lista_2, lista_3);
          // una vez agregados se vuelve a la instancia de eleccion
          menu(lista_1, lista_2, lista_3);
          break;
      }
    // muestra listas ordenadas
    case 2:
        cout << "LISTA 1 ORDENADA" << endl;
        lista_1->imprimir_lista();

        cout << "LISTA 2 ORDENADA" << endl;
        lista_2->imprimir_lista();

        cout << "LISTA 3 ORDENADA" << endl;
        lista_3->imprimir_lista();

        menu(lista_1, lista_2, lista_3);
        break;

    case 3:
        cout << "HASTA PRONTO :)" << endl;
        break;
  }
}

int main(int argc, char const *argv[]) {
  // se crean las listas
  Lista *lista_1 = new Lista();
  Lista *lista_2 = new Lista();
  Lista *lista_3 = new Lista();
  int opcion;

  menu(lista_1, lista_2, lista_3);
  return 0;
}
